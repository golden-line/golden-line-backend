resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = local.vpc_id
  tags = {
    Name = "internet_gateway"
  }
}

resource "aws_route_table" "route_table" {
  vpc_id = local.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }
}

resource "aws_route_table_association" "subnet_route" {
  subnet_id      = local.subnet_id1
  route_table_id = aws_route_table.route_table.id
}

resource "aws_route_table_association" "subnet2_route" {
  subnet_id      = local.subnet_id2
  route_table_id = aws_route_table.route_table.id
}

resource "aws_route_table_association" "subnet3_route" {
  subnet_id      = local.subnet_id3
  route_table_id = aws_route_table.route_table.id
}

resource "aws_security_group" "security_group" {
  name = "ecs-security-group"
  vpc_id = local.vpc_id

  ingress {
    from_port = 0
    to_port = 0
    protocol = -1
    self = "false"
    cidr_blocks = ["0.0.0.0/0"]
    description = "any"
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ecs_cluster" "golden_line_ecs_cluster" {
  name = "golden-line-ecs-cluster"

  setting {
    name  = "containerInsights"
    value = "disabled"
  }
}
