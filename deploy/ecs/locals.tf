locals {
  vpc_id     = "vpc-03f34d08b594505e3"
  cidr_block = "172.31.0.0/16"
  subnet_id1 = "subnet-0b0dfc5b0e6b8893e"
  subnet_id2 = "subnet-0534247bcb6ebad97"
  subnet_id3 = "subnet-011caf7816d288e7c"
}
