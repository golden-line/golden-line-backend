terraform {
  backend "s3" {
    bucket = "golden-line-terraform-state"
    key    = "ecs/terraform.tfstate"
    region = "eu-west-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0.0, < 6.0.0"
    }
  }
}

provider "aws" {
  region     = "eu-west-1"
}