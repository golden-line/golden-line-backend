#!/bin/bash

function random_number_between() {
  min=$1
  max=$2
  echo $(( RANDOM % (max - min + 1 ) + min ))
}

# Define API endpoint
API_ENDPOINT="http://ec2-34-243-124-35.eu-west-1.compute.amazonaws.com:8080/api/v1/customers"

# Define sociProfessionalCategory options
COLLECTION_CATEGORIES=("FRUTS" "VEGETABLES" "HOME")
SOCI_PROF_CATEGORIES=("BLUE_COLLAR" "WHITE_COLLAR" "STUDENT" "RETIRED" "UNEMPLOYED")

# Loop 1000 times
for i in {1..200}
do
  # Select a random sociProfessionalCategory
  SOCI_PROF_CATEGORY=${SOCI_PROF_CATEGORIES[$RANDOM % ${#SOCI_PROF_CATEGORIES[@]}]}

  COLLECTION_CATEGORY_1=${COLLECTION_CATEGORIES[$RANDOM % ${#COLLECTION_CATEGORIES[@]}]}
  COLLECTION_CATEGORY_2=${COLLECTION_CATEGORIES[$RANDOM % ${#COLLECTION_CATEGORIES[@]}]}

  RANDOM_NUMBER_1=$(random_number_between 1 100)
  RANDOM_NUMBER_2=$(random_number_between 1 100)
  TOTAL=$(($RANDOM_NUMBER_1 + $RANDOM_NUMBER_2))

  # Define customer data
  CUSTOMER_DATA="{
    \"childrenNumber\": 1,
    \"sociProfessionalCategory\": \"$SOCI_PROF_CATEGORY\",
    \"basketPrice\": $TOTAL,
    \"date\": 0,
    \"collection\": {
      \"totalBasketPrice\": $TOTAL,
      \"collectionDetails\": [
        {
          \"category\": \"$COLLECTION_CATEGORY_1\",
          \"basketPrice\": $RANDOM_NUMBER_1
        },
        {
          \"category\": \"$COLLECTION_CATEGORY_2\",
          \"basketPrice\": $RANDOM_NUMBER_2
        }
      ]
    }
  }"

  # Make POST request
  curl -X POST -H "Content-Type: application/json" -d "$CUSTOMER_DATA" $API_ENDPOINT
  sleep 0.1
done