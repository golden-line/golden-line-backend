package com.goldenline.goldenline.controller

import com.goldenline.goldenline.customer.Customer
import com.goldenline.goldenline.customer.CustomerService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@CrossOrigin(origins = ["*"])
@RestController
@RequestMapping("/api/v1/customers")
class CustomerController(private val customerService: CustomerService) {

    data class CustomersDto(val customers: List<Customer>) {}

    @GetMapping
    fun getCustomers(): CustomersDto {
        return CustomersDto(customerService.getCustomers())
    }

    @GetMapping("/expenditure-by-soci-professional-category")
    fun getExpenditureBySociProfessionalCategory(): List<Pair<String, Double>> {
        return customerService.getCustomers()
            .groupBy { it.sociProfessionalCategory }
            .map { (category, customers) -> category to customers.sumOf { it.basketPrice } }
    }

    @GetMapping("/mean-expenditure-by-soci-professional-category")
    fun getMeanExpenditureBySociProfessionalCategory(): List<Pair<String, Double>> {
        return customerService.getCustomers()
            .groupBy { it.sociProfessionalCategory }
            .map { (category, customers) -> category to customers.sumOf { it.basketPrice } / customers.size }
    }

    @GetMapping("/{id}")
    fun getCustomerById(@PathVariable id: Long): Customer {
        return customerService.getCustomerById(id)
    }

    @PostMapping
    fun createCustomer(@RequestBody customer: Customer): Customer {
        return customerService.createCustomer(customer)
    }

    @PutMapping("/{id}")
    fun updateCustomer(@PathVariable id: Long, @RequestBody customer: Customer): Customer {
        return customerService.updateCustomer(id, customer)
    }

    @DeleteMapping("/{id}")
    fun deleteCustomer(@PathVariable id: Long) {
        customerService.deleteCustomer(id)
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CustomerService.CustomerNotFoundException::class)
    fun handleCustomerNotFoundException(exception: CustomerService.CustomerNotFoundException): String {
        return exception.message.let { it ?: "Customer not found" }
    }

}