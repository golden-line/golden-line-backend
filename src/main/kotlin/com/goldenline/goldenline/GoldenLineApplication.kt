package com.goldenline.goldenline

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GoldenLineApplication

fun main(args: Array<String>) {
	runApplication<GoldenLineApplication>(*args)
}
