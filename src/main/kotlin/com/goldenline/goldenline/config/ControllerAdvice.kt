package com.goldenline.goldenline.config

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.sql.SQLException

data class ErrorResponse(val status: Int, val errorType: String, val message: String)

@ControllerAdvice
class ControllerAdvice {

    @ExceptionHandler(SQLException::class)
    protected fun handleSQLException(e: SQLException): ResponseEntity<ErrorResponse> {
        return ResponseEntity.status(500).body(
            ErrorResponse(400, HttpStatus.BAD_REQUEST.name,"${e.errorCode}: ${e.message}")
        )
    }

    @ExceptionHandler(Exception::class)
    protected fun handleException(e: Exception): ResponseEntity<ErrorResponse> {
        return ResponseEntity.status(500).body(ErrorResponse(
            500, HttpStatus.INTERNAL_SERVER_ERROR.name,e.message ?: "Internal server error")
        )
    }
}