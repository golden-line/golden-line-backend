package com.goldenline.goldenline.auth

data class LoginResponse(
    val token: String,
    val role: String,
    val expireIn: Long,
)
