package com.goldenline.goldenline.auth

data class RegisterUserDto(
    val email: String,
    val password: String,
    val fullName: String
) {
}