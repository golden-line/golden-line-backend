package com.goldenline.goldenline.auth

import com.goldenline.goldenline.jwt.JwtService
import com.goldenline.goldenline.user.User
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/api/v1/auth")
@RestController
class AuthenticationController(
    private val jwtService: JwtService,
    private val authenticationService: AuthenticationService
) {
    @PostMapping("/signup")
    fun register(@RequestBody registerUserDto: RegisterUserDto?): ResponseEntity<User> {
        val registeredUser: User = authenticationService.signup(registerUserDto!!)

        return ResponseEntity.ok(registeredUser)
    }

    @PostMapping("/login")
    fun authenticate(@RequestBody loginUserDto: LoginUserDto?): ResponseEntity<LoginResponse> {
        val authenticatedUser: User = authenticationService.authenticate(loginUserDto!!)

        val jwtToken = jwtService.generateToken(authenticatedUser)

        val loginResponse = LoginResponse(jwtToken, authenticatedUser.role, jwtService.expirationTime)

        return ResponseEntity.ok(loginResponse)
    }
}