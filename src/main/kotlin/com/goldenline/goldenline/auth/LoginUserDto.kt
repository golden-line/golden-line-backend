package com.goldenline.goldenline.auth

data class LoginUserDto(
    val email: String,
    val password: String
)
