package com.goldenline.goldenline.auth

import com.goldenline.goldenline.user.User
import com.goldenline.goldenline.user.UserRepository
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class AuthenticationService(
    private val userRepository: UserRepository,
    private val authenticationManager: AuthenticationManager,
    private val passwordEncoder: PasswordEncoder
) {
    fun signup(input: RegisterUserDto): User {
        val user = User(
            null,
            fullName = input.fullName,
            email = input.email,
            password = passwordEncoder.encode(input.password),
            null,
            null
        )

        return userRepository.save(user)
    }

    fun authenticate(input: LoginUserDto): User {
        authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(
                input.email,
                input.password
            )
        )

        return userRepository.findByEmail(input.email) ?: throw IllegalArgumentException("Invalid credentials")
    }

}