package com.goldenline.goldenline.user

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface UserRepository: CrudRepository<User, Int> {

    fun findByEmail(email: String): User?

    @Modifying
    @Transactional
    @Query("UPDATE User u SET u.active = true WHERE u.email = ?1")
    fun activateAccount(email: String)

    @Modifying
    @Transactional
    @Query("UPDATE User u SET u.active = false WHERE u.email = ?1")
    fun deactivateAccount(email: String)

    @Query("SELECT u FROM User u WHERE u.active = false")
    fun getNotActivatedUsers(): List<User>
}