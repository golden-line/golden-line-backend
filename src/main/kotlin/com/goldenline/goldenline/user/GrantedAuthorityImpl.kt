package com.goldenline.goldenline.user

import org.springframework.security.core.GrantedAuthority

class GrantedAuthorityImpl(private val role: Role): GrantedAuthority {
    override fun getAuthority(): String {
        return role.name
    }
}