package com.goldenline.goldenline.user

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.Date

enum class Role() {
    ROLE_MARKETING,
    ROLE_ADMIN;
    override fun toString(): String {
        return name
    }
}

@Entity
@Table(name = "users")
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    val id: Long?,

    @Column(unique = true, length = 100, nullable = false)
    val fullName: String,

    @Column(unique = true, length = 100, nullable = false)
    val email: String,

    @JsonIgnore
    @Column(nullable = false)
    private val password: String,

    @CreationTimestamp
    @Column(updatable = false, name = "created_at")
    val createdAt: Date?,

    @UpdateTimestamp
    @Column(name = "updated_at")
    val updateAt: Date?,

    @Column(nullable = false, name = "role", columnDefinition = "varchar(255) default 'ROLE_MARKETING'")
    val role: String = Role.ROLE_MARKETING.name,

    val active: Boolean = false,

    val accountNonExpired: Boolean = true

): UserDetails {

    override fun getAuthorities(): Collection<GrantedAuthority> {
        return listOf(GrantedAuthorityImpl(Role.valueOf(role)))
    }

    override fun getPassword(): String {
        return password
    }

    override fun getUsername(): String {
        return email
    }

    override fun isEnabled(): Boolean {
        return active
    }

    override fun isAccountNonExpired(): Boolean {
        return accountNonExpired
    }

}