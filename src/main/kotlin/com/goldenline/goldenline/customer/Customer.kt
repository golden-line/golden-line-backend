package com.goldenline.goldenline.customer

import com.goldenline.goldenline.collection.Collection
import jakarta.persistence.*
import org.springframework.context.annotation.Lazy
import java.time.Instant

@Entity
@Table(name = "customers")
data class Customer (
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    val identifier: Long?,
    val childrenNumber: Int,
    val sociProfessionalCategory: String,
    val basketPrice: Double,
    val date: Instant,
    @OneToOne(targetEntity = Collection::class, cascade = [CascadeType.ALL])
    @Lazy
    val collection: Collection
)
