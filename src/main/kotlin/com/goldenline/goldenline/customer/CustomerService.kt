package com.goldenline.goldenline.customer

import org.springframework.stereotype.Service

@Service
class CustomerService(private val customerRepository: CustomerRepository) {

    fun getCustomerById(id: Long): Customer {
        return customerRepository.findById(id).orElseThrow { CustomerNotFoundException(id) }
    }

    fun getCustomers(): List<Customer> {
        return customerRepository.findAll().toList()
    }

    fun createCustomer(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    fun updateCustomer(id: Long, customer: Customer): Customer {
        if (!customerRepository.existsById(id)) {
            throw CustomerNotFoundException(id)
        }
        return customerRepository.save(customer)
    }

    fun deleteCustomer(id: Long) {
        if (!customerRepository.existsById(id)) {
            throw CustomerNotFoundException(id)
        }
        customerRepository.deleteById(id)
    }

    class CustomerNotFoundException(id: Long) : RuntimeException("Customer with id $id not found")
}