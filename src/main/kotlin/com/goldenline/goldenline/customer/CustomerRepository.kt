package com.goldenline.goldenline.customer;

import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer, Long>
