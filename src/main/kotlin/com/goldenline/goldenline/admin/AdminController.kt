package com.goldenline.goldenline.admin

import com.goldenline.goldenline.user.User
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/api/v1/admin")
@RestController
class AdminController(
    private val adminService: AdminService
) {
    @PostMapping("/users/activate")
    fun activateAccount(@RequestBody request: ActivateUserRequest): ResponseEntity<Unit> {
        adminService.activateAccount(request.email)
        return ResponseEntity.ok().build()
    }

    @PostMapping("/users/deactivate")
    fun deactivateAccount(@RequestBody request: ActivateUserRequest): ResponseEntity<Unit> {
        adminService.deactivateAccount(request.email)
        return ResponseEntity.ok().build()
    }

    @GetMapping("/users")
    fun getUsers(): ResponseEntity<List<User>> {
        return ResponseEntity.ok(adminService.getUsers())
    }

}

data class ActivateUserRequest(
    val email: String
)