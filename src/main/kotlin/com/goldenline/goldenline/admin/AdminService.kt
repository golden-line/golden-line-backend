package com.goldenline.goldenline.admin

import com.goldenline.goldenline.user.User
import com.goldenline.goldenline.user.UserRepository
import org.springframework.stereotype.Service

@Service
class AdminService(
    private val userRepository: UserRepository
) {
    fun activateAccount(email: String): Unit {
        val user = userRepository.findByEmail(email) ?: throw IllegalArgumentException("User not found")
        userRepository.activateAccount(user.email)
    }

    fun deactivateAccount(email: String): Unit {
        val user = userRepository.findByEmail(email) ?: throw IllegalArgumentException("User not found")
        userRepository.deactivateAccount(user.email)
    }

    fun getUsers(): List<User> {
        return userRepository.findAll().toList()
    }
}