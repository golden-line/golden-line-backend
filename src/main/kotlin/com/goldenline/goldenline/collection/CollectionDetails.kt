package com.goldenline.goldenline.collection

data class CollectionDetails(
    val category: String,
    val basketPrice: Double,
) {

}