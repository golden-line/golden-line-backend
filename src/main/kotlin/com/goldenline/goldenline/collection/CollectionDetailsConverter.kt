package com.goldenline.goldenline.collection

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import jakarta.persistence.AttributeConverter

class CollectionDetailsConverter: AttributeConverter<List<CollectionDetails>, String> {

    companion object {
        private val mapper = ObjectMapper().registerModule(
            KotlinModule.Builder().build()
        )
    }

    override fun convertToDatabaseColumn(details: List<CollectionDetails>?): String {
        try {
            return mapper.writeValueAsString(details)
        } catch (e: Exception) {
            throw RuntimeException("Error converting collection details to JSON", e)

        }
    }

    override fun convertToEntityAttribute(p0: String?): List<CollectionDetails> {
        try {
            return mapper.readValue(p0, Array<CollectionDetails>::class.java).toList()
        } catch (e: Exception) {
            throw RuntimeException("Error converting JSON to collection details", e)
        }
    }
}