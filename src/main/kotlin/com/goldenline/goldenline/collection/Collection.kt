package com.goldenline.goldenline.collection

import jakarta.persistence.*

@Entity
@Table(name = "collections")
data class Collection(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    val identifier: Long?,
    val totalBasketPrice: Double,
    @Convert(converter = CollectionDetailsConverter::class)
    val collectionDetails: List<CollectionDetails>
)